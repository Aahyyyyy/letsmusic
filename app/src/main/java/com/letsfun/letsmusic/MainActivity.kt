package com.letsfun.letsmusic

import android.os.Bundle
import android.view.View
import com.letsfun.letsmusic.databinding.ActivityMainBinding
import com.letsfun.letswidget.sdk.core.base.BaseViewBindingActivity

class MainActivity : BaseViewBindingActivity<ActivityMainBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v.letsTitleBar.ivNavigation.visibility = View.GONE

    }

    override fun getPageTitle(): String {
        return "LetsMusic"
    }

    override fun getViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }
}